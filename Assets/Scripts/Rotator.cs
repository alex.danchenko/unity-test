﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    private float rotationSpeed = 60f;

    void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        float newYAngle = transform.localEulerAngles.y + Time.deltaTime * rotationSpeed;
        Vector3 targetRotation = new Vector3(0f, newYAngle, 0f);
        
        transform.localEulerAngles = targetRotation;
    }
}
